// typescript 4.9
// TypeScript developers are often faced with a dilemma: 
// we want to ensure that some expression matches some type, but also want to keep the most specific type of that 
// expression for inference purposes.
// the new satisfies operator lets us validate that the type of an expression matches some type, 
// without changing the resulting type of that expression. As an example, we could use satisfies to validate 
// that all the properties of palette are compatible with string | number[]:

//Custom interface for rendering images
interface ICustomImage {
  data: string;
  width: number;
  height: number;
}

//Sample of a Custom Image
const myCustomImage: ICustomImage = {
  data: "base64",
  width: 200,
  height: 150,
};

//Image type for the user
type UserImage = string | ICustomImage;

//User interface
interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  image: UserImage;
}

//Bad ❌
const badUser = {
  id: 1,
  firstName: "Alex",
  lastName: "Brooks",
  image: "image-url",
} as IUser;

// badUser.

//Good ✅
const goodUser = {
  id: 1,
  firstName: "Alex",
  lastName: "Brooks",
  image: myCustomImage,
} satisfies IUser;

// goodUser.image.
