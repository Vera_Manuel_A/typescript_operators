type Species = "cat" | "dog";

interface Pet {
  species: Species;
}

class Cat implements Pet {
  public species: Species = "cat";
  public meow(): void {
    console.log("Mauyo Mauyo");
  }

  public jump(): void {
    console.log("Salto...");
  }

  public askFood(): void {
    console.log("Dame Comida Humano!!!!...");
  }
}

function petIsCat(pet: Pet): pet is Cat {
  return pet.species === "cat";
}

function petIsCatBoolean(pet: Pet): boolean {
  return pet.species === "cat";
}

const p: Pet = new Cat();

//❌
// p.meow();.

if (petIsCatBoolean(p)) {
  // p.meow(); 

  (p as Cat).meow();

}

// ✅
if (petIsCat(p)) {
  p.meow(); 
}
