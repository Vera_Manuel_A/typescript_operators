import fetch from "node-fetch";

let userAny: any; ///< ❌ Simply means turn off the Typescript "type-checks"
let userUnkown: unknown; ///< ✅ 

// userAny.myThing();
// userUnkown.myThing(); 

interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  image: string;
  age: number;
}

interface IAdminUser extends IUser {
  token: string;
  addNewUser: () => void;
}

function isAdminUser(object: unknown): object is IAdminUser {
  if (object !== null && typeof object === "object") {
    return "token" in object;
  }

  return false;
}

function isRegularUser(object: unknown): object is IUser {
  if (object !== null && typeof object === "object") {
    return "token" !in object;
  }

  return false;
}

async function fetchUser() {
  const response = await fetch("https://dummyjson.com/users/1");

  //Bad ❌
  const badUser = await response.json();
  // badUser.

  //Good ✅
  const goodUser: unknown = await response.json();

//   if (isAdminUser(goodUser)) {
//     goodUser.
//   }

//   if(isRegularUser(goodUser)) {
//     goodUser.
//   }
}


// otro ejemplo

type Route = { path: string; children?: Routes}
type Routes = Record<string, Route> // string key and value Route

const routes = {
  AUTH: {
    path: "/auth", 
    children: {
      LOGIN: {
        path: "/login"
      }
    }
  },
  HOME: {
    path: "/"
  }
} satisfies Routes;



export {}