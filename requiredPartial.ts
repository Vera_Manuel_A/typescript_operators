// Required

interface FormInputs {
    name?: string;
    lastName?: string;
    email: string;
    password: string;
    address1: string;
    address2?: string;
}

const inputs: FormInputs = {
    email: 'blab@ble.com',
    password: '1234',
    address1: 'mi casa',
}

//Bad ❌ - DRY
interface FormInputsRequired {
    name: string;
    lastName: string;
    email: string;
    password: string;
    address1: string;
    address2: string;
}

//Good ✅

type MyFormInputRequired = Required<FormInputs>

const requiredInputs: MyFormInputRequired = {
    name: 'string',
    lastName: 'string',
    email: 'string',
    password: 'string',
    address1: 'string',
    address2: 'string',
}


// Partial

interface VideoGame {
    title: string;
    description: string;
    ageRating: '3+' | '10+' | '16+'
}


type VideoGamePartial = Partial<VideoGame>

const playStationGame: VideoGamePartial = {
    title: 'God of War 2018', 
}


interface NintendoGame extends Partial<VideoGame> {
    plataform: 'nes' | 'snes' | 'N64';
}

const nintendoGame: NintendoGame = {
    title: 'Mario Bros',
    plataform: 'nes'
}


