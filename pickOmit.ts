// Pick

interface Consumable {
    size: 'small' | 'medium' | 'large';
    calories: number;
    millilitres: number;
    grams: number;
}

// type Pizza = Pick<Consumable, "size" | "calories">

// const champiñonesPizza: Pizza = {
//     size: 'small',
//     calories: 500
// }

interface Pizza extends Pick<Consumable, "size" | "calories"> {
    toppings: string[];
}

const pineapplePizza : Pizza = {
    size: 'medium',
    calories: 800,
    toppings: ['panceta', 'piña']
}

// omit

// type Sandwich = Omit<Consumable, 'millilitres'>;

// const sandwichJamon: Sandwich = {
//     size: 'large',
//     calories: 1000,
//     grams: 600,
// }

interface Sandwich extends Omit<Consumable, "millilitres"> {
    fillings: string[];
}

const baconSandwich: Sandwich = {
    size: 'small',
    calories: 200,
    grams: 100,
    fillings: ['bacon', 'ketchup']
}