interface CatInfo {
    age: number;
    breed: string;
}

type CatName = 'garfield' | 'levi' | 'asesino';

const cats : Record<CatName, CatInfo> = {
    garfield: { age: 5, breed: 'callejero' },
    levi: { age: 10, breed: 'persian'},
    asesino: { age: 1, breed: 'maligno'},
}


// cats.